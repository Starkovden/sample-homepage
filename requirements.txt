# ablog breaks with later Sphinx versions
Sphinx==1.5.6
ablog==0.8.4
invoke>=0.21.0
invocations>=0.20
# Sphinx 1.5.6 breaks with later docutils versions
docutils==0.13.1

